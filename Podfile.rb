source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '10.0'
use_frameworks!

target 'Beauty Cats' do
    pod 'lottie-ios'
    pod 'SPLarkController'
    pod 'PMSuperButton'
    pod 'VerticalCardSwiper'
    

end
