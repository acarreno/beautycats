//
//  WeightModel.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
struct WeightModel:Codable {
    var imperial:String
    var metric:String
}
