//
//  BreedsViewController.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import UIKit

class BreedsViewController: UIViewController{
    
    
    @IBOutlet weak var table: UITableView!
    
  
    var vm=BreedViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vm.delegate=self
        vm.getBreeds()
        
        //Services.get(url: ApiVars.get.breeds, params: ["":""])
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BreedsViewController:BreedViewModelDelegate{
    
    func reloadData() {
        DispatchQueue.main.async {
            self.table.reloadData()
        }
    }
    func removeCell(index:Int){
         DispatchQueue.main.async {
            let indexPath = IndexPath(item: index, section: 0)
            self.table.deleteRows(at: [indexPath], with: .fade)
        }
    }
    func alert(msj: String) {
        print(msj)
    }
    
    
}
extension BreedsViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell.rawValue, for: indexPath) as! BreedsCellsTableViewCell
        

        switch item.cell {
        case .pagination:
            cell.contentView.addLoading()
            if tableView.isLast(for: indexPath) && indexPath.row != 0 && vm.isLoading{
                
                    vm.getBreeds()
               
                
            }
        case .item:
            cell.lblName.text=item.obj!.name
        }
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //vm.getDetail(id: vm.container[indexPath.row].obj!.id)
        goToCat(breed:vm.container[indexPath.row].obj! )
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item=vm.container[indexPath.row]
        
        return CGFloat(item.height)
        
    }
    
}
