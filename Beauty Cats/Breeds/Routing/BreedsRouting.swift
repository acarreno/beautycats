//
//  BreedsRouting.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
import UIKit

extension BreedsViewController{
    static func instantiate() -> BreedsViewController {
        let storyboard = UIStoryboard(name: "Breeds", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! BreedsViewController
        return vc
    }
    func goToCat(breed:BreedModel){
       let vc = CatViewController.instantiate(breed: breed)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
