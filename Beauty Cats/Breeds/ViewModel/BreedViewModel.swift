//
//  BreedViewModel.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
protocol BreedViewModelDelegate: class {
    func reloadData()
    func alert(msj:String)
    func removeCell(index:Int)
}
class BreedViewModel {
    
    enum ids:String {
        case item="item"
        case pagination="pagination"
        
    }
    
    struct cellModel {
        let cell:ids
        let obj:BreedModel?
        var height:Int
    }
    
    weak var delegate: BreedViewModelDelegate?
    let manager=BreedsManager()
    var container=[cellModel]()
    var page=0
    var isLoading=false
   
    
    func getBreeds(){
        page += 1
        if page==1{
            self.container.append(cellModel(cell:.pagination,obj:nil,height:200))
        }
        isLoading=false
        manager.getBreeds(page: page, susses: { (list) in
           
            let removeIndex=self.container.count-1
            
            self.container[removeIndex].height=0
            
            list.forEach { (item) in
                self.container.append(cellModel(cell:.item,obj:item,height:56))
                
            }
            if list.count==0{
                self.container[removeIndex].height=0
                self.isLoading=false
                
            }else{
                self.container.append(cellModel(cell:.pagination,obj:nil,height:200))
                self.isLoading=true
                
            }
            self.delegate?.reloadData()
            
            
        }) { (error) in
            self.delegate?.alert(msj: error)
        }
    }
    func getDetail(id:String){
        manager.getDetail(id: id, susses: { (item) in
            
        }) { (error) in
            self.delegate?.alert(msj: error)
        }
    }
}
