//
//  BreedsManager.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
class BreedsManager {
    
    func getBreeds(page:Int,susses:@escaping ([BreedModel])->(),failed:@escaping (String)->()){
        let url="\(ApiVars.get.breeds)?attach_breed=1&page=\(page)&limit=20"
        Services.get(url: url, susses: { (res) in
            if let list = res as? [[String:Any]]{
                var parcList:[BreedModel]=[BreedModel]()
                list.forEach { (item) in
                    parcList.append(try! DictionaryDecoder().decode(BreedModel.self, from: (item) ))
                }
                
                susses(parcList)
            }else{
                failed("Error, try again")
            }
        }) { (error) in
            failed(error)
        }
    }
    
    func getDetail(id:String,susses:@escaping ([BreedModel])->(),failed:@escaping (String)->()){
        
        
        let url="\(ApiVars.get.search)?breed_id=\(id)"
        Services.get(url: url, susses: { (res) in
            if let list = res as? [[String:Any]]{
                var parcList:[BreedModel]=[BreedModel]()
                list.forEach { (item) in
                    parcList.append(try! DictionaryDecoder().decode(BreedModel.self, from: (item) ))
                }
                
                susses(parcList)
            }else{
                failed("Error, try again")
            }
        }) { (error) in
            failed(error)
        }
    }
    
}
