//
//  HomeViewModel.swift
//  Beauty Cats
//
//  Created by iMac on 2/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//


import Foundation

class HomeViewModel {
    
    
   enum ids {
       case breeds
       case history
       case match
   }
    
    typealias CellModel =  (id:ids,image:String,title:String)

    var items = [CellModel]()
    
    func inflateItems(){
        items.append(
            (
                id:ids.breeds,
                image:"pet-bottle",
                title:"Breeds"
            )
        )
        items.append(
            (
                id:ids.match,
                image:"pets",
                title:"Cats"
            )
        )
        items.append(
            (
                id:ids.history,
                image:"history",
                title:"History"
            )
        )
        
       
    }
    
    
    
    
    
    
    
}

