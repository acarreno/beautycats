//
//  HomeViewController.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var tab: UITabBar!
    @IBOutlet weak var tabButtonList: UITabBarItem!
    @IBOutlet weak var contentVC: UIView!
    
    enum ids {
        case breeds
        case history
        case match
    }
    
    
    typealias CellModel =  (id:ids,image:String,title:String)

    
    var vm=HomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetUp()
        tab.delegate=self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideBackButton()
    }
    func initSetUp(){
        vm.inflateItems()
        configTab()
        changeView(controller: BreedsViewController.instantiate())
        hideBackButton()
        
    }
    
    
    func configTab(){
        
        tab.items=[]
        for (index,item) in vm.items.enumerated() {
            let itemButton=UITabBarItem()
            itemButton.tag=index
             itemButton.image = UIImage(named: item.image)
             itemButton.badgeColor = #colorLiteral(red: 0.2588235294, green: 0.1725490196, blue: 0.3137254902, alpha: 1)
             itemButton.title=item.title
            itemButton.setTitleTextAttributes([NSAttributedString.Key.font:UIFont(name: "Helvetica", size: 14)!], for: .normal)
             itemButton.selectedImage = UIImage(named: item.image)
             tab.items?.append(itemButton)
        }
        
        
    }
    func changeView(controller:UIViewController){
        for (index,_) in contentVC.subviews.enumerated() {
            contentVC.subviews[index].removeFromSuperview()
        }
        addChild(controller)
        
        controller.view.frame=contentVC.bounds
        contentVC.addSubview(controller.view)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HomeViewController:UITabBarDelegate{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let index:Int=item.tag
        let item=vm.items[index]
        var vc:UIViewController=UIViewController()
        
        switch item.id {
            case .history:
                vc=HistoryViewController.instantiate()
            case .breeds:
                vc=BreedsViewController.instantiate()
            case .match:
                vc=MatchViewController.instantiate()
       
        }
        changeView(controller: vc)
    }
}
