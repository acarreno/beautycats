//
//  HomeRouting.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation

import Foundation
import UIKit


extension HomeViewController{
    
    static func instantiate() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
        return vc
    }
    
    
}

