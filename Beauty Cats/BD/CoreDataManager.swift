//
//  BDManager.swift
//  Beauty Cats
//
//  Created by iMac on 2/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//


import Foundation
//1
import CoreData
class CoreDataManager {
    //2
    private let container : NSPersistentContainer!
    //3
    init() {
        container = NSPersistentContainer(name: "BD")
        
        setupDatabase()
    }
    
    private func setupDatabase() {
        
            container.loadPersistentStores { (desc, error) in
            if let error = error {
                print("Error loading store \(desc) — \(error)")
                return
            }
            print("Database ready!")
        }
    }
    func createRegister(id:String,url:String,isLike:Bool, completion: @escaping() -> Void) {
        // 2
        let context = container.viewContext
      
        // 3
        let history = History(context: context)
        
        history.id=id
        history.urlImagen=url
        history.date=Date()
        history.isLike=isLike
        // 5
        do {
            try context.save()
            print("Usuario \(history.id!) guardado")
            completion()
        } catch {
         
          print("Error guardando usuario — \(error)")
        }
    }
    
    func getHistoryList() -> [History] {
        //1
        let fetchRequest : NSFetchRequest<History> = History.fetchRequest()
        do {
      
            //2
            let result = try container.viewContext.fetch(fetchRequest)
            return result
        } catch {
            print("error getting history list \(error)")
         }
     
          //3
         return []
    }
}
