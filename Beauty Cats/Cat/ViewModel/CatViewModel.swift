//
//  CatViewModel.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
protocol CatViewModelDelegate: class {
    func loadData()
    func reloadData()
    func alert(msj:String)
   
}
class CatViewModel {
    
    enum itemKeys:String {
        case origin="origin"
        case energy_level="energy_level"
        case life_span="life_span"
        case child_friendly="child_friendly"
        case dog_friendly="dog_friendly"
        case affection_level="affection_level"
        case description="description"
    }
    
    struct CellModel {
        var key:itemKeys
        var val:Any
        var image:String
        var showName:String
        var active:Bool
        
    }
    
    var container=[CellModel]()
    let manager = CatManager()
    weak var delegate:CatViewModelDelegate?
    var breed:BreedModel?
    var catDetail:CatModel?
    
    
    
    func getDetail() {
        
        manager.getDetail(id: breed!.id, susses: { (item) in
            self.catDetail=item
            self.delegate?.loadData()
        }) { (error) in
            self.delegate?.alert(msj: error)
        }
    }
    
    func prepareDataToCollection(){
        container.append(CellModel(key: .description, val: breed!.description, image: "description", showName: "Description",active:true))
        container.append(CellModel(key: .energy_level, val:breed!.energy_level, image: "energy_level", showName: "Energy level",active:false))
        container.append(CellModel(key: .life_span, val: breed!.life_span, image: "life_span", showName: "Life span",active:false))
        container.append(CellModel(key: .child_friendly, val: breed!.child_friendly, image: "child_friendly", showName: "Chil friendly",active:false))
        container.append(CellModel(key: .dog_friendly, val: breed!.dog_friendly, image: "dog_friendly", showName: "Dog friendly",active:false))
        container.append(CellModel(key: .affection_level, val: breed!.affection_level, image: "affection_level", showName: "Affection level",active:false))
        container.append(CellModel(key: .origin, val: breed!.origin, image: "origin", showName: "Origin",active:false))
        
    }
    
    func activeOrInactive(index:Int){
        
        for (newIndex, _) in container.enumerated() {
            container[newIndex].active=false
        }
        container[index].active=true
        self.delegate?.reloadData()
    }
}
