//
//  CatManager.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
class CatManager {
    
    func getDetail(id:String,susses:@escaping (CatModel)->(),failed:@escaping (String)->()){
        
        let url="\(ApiVars.get.search)?breed_id=\(id)"
        Services.get(url: url, susses: { (res) in
            if let cat = res as? [[String:Any]]{
                if cat.count>0{
                    susses(try! DictionaryDecoder().decode(CatModel.self, from: (cat[0]) ))
                }else{
                    failed("Not found the breef")
                }
                
            }else{
                failed("Error, try again")
            }
        }) { (error) in
            failed(error)
        }
    }
    
}
