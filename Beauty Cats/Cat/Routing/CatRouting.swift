//
//  Cat.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
import UIKit

extension CatViewController{
    static func instantiate(breed:BreedModel) -> CatViewController {
        let storyboard = UIStoryboard(name: "Cat", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CatViewController
        vc.vm.breed=breed
        return vc
    }
    
}
