//
//  CatViewController.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import UIKit

class CatViewController: UIViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var catImage: UIImageView!
    @IBOutlet weak var textArea: UITextView!
     @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var btnMas: UIButton!
    
    let vm=CatViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.delegate=self
        vm.getDetail()
        initSetUp()
        // Do any additional setup after loading the view.
    }
    func initSetUp(){
        lblName.text=vm.breed!.name
        vm.prepareDataToCollection()
        collection.reloadData()
        collection.delegate = self
        catImage.addRadius(round: catImage.frame.width/2)
        catImage.addShadow()
        pritInformation(index:0)
        
        
        textArea.sizeToFit()
    }
    
    func pritInformation(index:Int){
        
        let item = vm.container[index]
        var html = ""
        var level = ""
        switch  item.key{
        case .description:
            
            html="<p style='text-align:center;font-size:15px;font-family:Helvetica'>\(item.val)</p>"
            
        case .energy_level:
            
            level="Low"
            if item.val as! Int > 3 {
                level="High"
            }
            
            html="<p style='text-align:center;font-size:25px;font-family:Helvetica;color:#422C50'>Level<br><span style='text-align:center;font-size:50px;font-family:Helvetica;'>\(item.val)</span><br> \(level)</p>"
            
        case .affection_level:
            
            level="Low"
            if item.val as! Int > 3 {
                level="High"
            }
            
            html="<p style='text-align:center;font-size:25px;font-family:Helvetica;color:#422C50'>Level<br><span style='text-align:center;font-size:50px!important;font-family:Helvetica'>\(item.val)</span><br> \(level)</p>"
            
        case .child_friendly:
            
             level="Embittered"
            if item.val as! Int > 3 {
                level="Friendly"
            }
            
            html="<p style='text-align:center;font-size:25px;font-family:Helvetica;color:#422C50'>Level<br><span style='text-align:center;font-size:50px!important;font-family:Helvetica'>\(item.val)</span><br> \(level)</p>"
            
        case.dog_friendly:
            
            level="Embittered"
            if item.val as! Int > 3 {
                level="Friendly"
            }
               
            html="<p style='text-align:center;font-size:25px;font-family:Helvetica;color:#422C50'>Level<br><span style='text-align:center;font-size:50px!important;font-family:Helvetica'>\(item.val)</span><br> \(level)</p>"
            
        case .life_span:
            
            html="<p style='text-align:center;font-size:25px;font-family:Helvetica;color:#422C50'>Years<br><span style='text-align:center;font-size:50px!important;font-family:Helvetica'>\(item.val)</span><br> </p>"
        case .origin:
        
            html="<p style='text-align:center;font-size:25px;font-family:Helvetica;color:#422C50'>Origin<br><span style='text-align:center;font-size:20px!important;font-family:Helvetica'>\(item.val)</span><br> </p>"
            
        
        }
        
        textArea.attributedText=html.htmlToAttributedString
    }
    
    @IBAction func moreInformation(_ sender: Any) {
        guard let url = URL(string: (vm.breed?.wikipedia_url!)!) else { return }
        UIApplication.shared.open(url)
    }
    
    @objc func tapButton(_ sender:UIButton){
        vm.activeOrInactive(index: sender.tag)
        pritInformation(index:sender.tag)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CatViewController:CatViewModelDelegate{
    func loadData() {
        DispatchQueue.main.async {
            self.catImage.load(strUrl: self.vm.catDetail!.url)
        }
    }
    func reloadData(){
        DispatchQueue.main.async {
            self.collection.reloadData()
        }
    }
    func alert(msj: String) {
        print(msj)
    }
    
    
}
extension CatViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let items=vm.container.count
        print(vm.container.count)
        return items
    }
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = vm.container[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CatCollectionViewCell
        cell.lblName.text=item.showName
        cell.btnIcon.setImage(UIImage(named:item.image), for: .normal)
        cell.btnIcon.tintColor = .black
        cell.btnIcon.tag=indexPath.row
        //cell.imageIcon.image=UIImage(named:item.image)
        cell.contentImage.addBoder()
        cell.contentImage.backgroundColor = .white
        cell.contentImage.addRadius(round: 10)
        cell.contentImage.frame.origin.y=22
        cell.lblName.frame.origin.y=22
        
        cell.btnIcon.addTarget(self, action: #selector(tapButton(_:)), for: .touchDown)
        
        if item.active{
            UIView.animate(withDuration: 0.5) { 
                cell.contentImage.backgroundColor = .black
                cell.btnIcon.tintColor = .white
                cell.contentImage.frame.origin.y=0
                cell.lblName.frame.origin.y=cell.contentView.frame.height-cell.lblName.frame.height
            }
        }
        //cell.imageIcon.backgroundColor = .white
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
      
        return CGSize(width: 89, height: 87);
    }
    
    func collectionView(_: UICollectionView, layout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt: Int) -> CGFloat{
        return 0
    }
    
    
}
