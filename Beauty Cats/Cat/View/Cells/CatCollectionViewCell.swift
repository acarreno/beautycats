//
//  CatCollectionViewCell.swift
//  Beauty Cats
//
//  Created by iMac on 2/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import UIKit

class CatCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var btnIcon: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var contentImage: UIView!
}
