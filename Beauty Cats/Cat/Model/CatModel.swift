//
//  CatModel.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
struct CatModel:Codable {
    var id:String
    var url:String
    var width:Int
    var height:Int
    var breeds:[BreedModel]
}
