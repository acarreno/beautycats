//
//  Dictionary.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
extension Dictionary {
    
    var json: String {
        let invalidJson = "{}"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self)
            let stringJSON = String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
            return stringJSON
        } catch {
            return invalidJson
        }
    }
    
    func printJson() {
        print(json)
    }
    
}
