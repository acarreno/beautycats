//
//  UIViewController.swift
//  Beauty Cats
//
//  Created by iMac on 2/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
import UIKit
import Lottie
extension UIViewController{
    func showLoading(){
        var animationView: AnimationView?
        animationView = .init(name: "coffee")
        animationView?.tag=999
        animationView!.frame = view.bounds
        
        // 3. Set animation content mode
        
        animationView!.contentMode = .scaleAspectFit
        
        // 4. Set animation loop mode
        
        animationView!.loopMode = .loop
        
        // 5. Adjust animation speed
        
        animationView!.animationSpeed = 0.5
        
        view.addSubview(animationView!)
        
        // 6. Play animation
        
        animationView!.play()
    }
    func hideBackButton(){
        self.navigationController?.navigationBar.backItem?.title=""
        self.navigationController?.navigationItem.backBarButtonItem?.title=""
        
    }
}
