//
//  ImageView.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
import UIKit
extension UIImageView{
    
    func downloadImage(from url: String) {
        print("Download Started")
        let urlParc=URL(string: url)
        print(urlParc!)
       
        URLSession.shared.dataTask(with: urlParc!) { (data, response, error) in
            
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? urlParc!.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() { [weak self] in
                self?.image = UIImage(data: data)
            }
        }
         
    }
    func load(strUrl: String) {
        let url=URL(string: strUrl)!
        self.addLoading()
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.removeLoading()
                        self?.image = image
                    }
                }
            }
        }
    }
    
    
}
