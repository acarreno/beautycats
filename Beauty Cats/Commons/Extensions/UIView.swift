//
//  UIView.swift
//  Beauty Cats
//
//  Created by iMac on 2/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
import UIKit
import Lottie

extension UIView{
    func addShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 2, height: 2)
        
    }
    func addBoder(){
        self.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.layer.borderWidth=1
        
    }
    func addRadius(round:CGFloat){
        self.layoutIfNeeded()
        layer.cornerRadius = round
        layer.masksToBounds = true

    }
    func addLoading(){
        var animationView: AnimationView?
        animationView = .init(name: "cat")
        animationView?.tag=999
        animationView!.frame = self.bounds
        animationView!.loopMode = .loop
        // 3. Set animation content mode
        
        animationView!.contentMode = .scaleAspectFit
        
        // 4. Set animation loop mode
        
        animationView!.loopMode = .loop
        
        // 5. Adjust animation speed
        
        animationView!.animationSpeed = 0.5
        
        self.addSubview(animationView!)
        
        // 6. Play animation
        
        animationView!.play()
    }
    func removeLoading(){
        self.subviews.forEach { (item) in
            item.removeFromSuperview()
        }
    }
}
