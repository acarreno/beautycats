//
//  Services.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
class Services {
    
    static let key = "9b3b1a3d-9094-4f73-819b-577dcae737bd"
    
    static func post(url:String,params:[String:Any]){
        
        
        let Url = "\(ApiVars.baseurl)\(url)"
        let parameterDictionary = params
        var request:URLRequest!
        guard let serviceUrl = URL(string: Url) else { return }
        
        request = URLRequest(url: serviceUrl)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields=["x-api-key":key]
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
            return
        }
        request.httpBody = httpBody

        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    static func get(url:String,susses:@escaping (Any)->(),failed:@escaping (String)->()){
        
        
        let Url = "\(ApiVars.baseurl)\(url)"
        
        var request:URLRequest!
        guard let serviceUrl = URL(string: Url) else { return }
        
        request = URLRequest(url: serviceUrl)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields=["x-api-key":key]
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        

        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    susses(json)
                    
                } catch {
                    print(error)
                    failed(error.localizedDescription)
                }
            }
        }.resume()
    }
    deinit {
        debugPrint("services out of memory")
    }
}
