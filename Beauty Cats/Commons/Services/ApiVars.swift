//
//  ApiVars.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation

struct ApiVars {
    static let baseurl="https://api.thecatapi.com"
    struct get {
        static let breeds="/v1/breeds"
        static let search="/v1/images/search"
        
    }
}
