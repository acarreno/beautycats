//
//  HistoryViewModel.swift
//  Beauty Cats
//
//  Created by iMac on 2/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
protocol HistoryViewModelDelegate: class {
    func loadData()
    func reloadData()
    func alert(msj:String)
   
}
class HistoryViewModel {
    
    
    
    struct CellModel {
        
        var val:Any
        var image:String
        var showName:String
        var active:Bool
        
    }
    
    var container=[History]()
    let manager = CoreDataManager()
    weak var delegate:HistoryViewModelDelegate?
   
    func getHistory(){
        container=manager.getHistoryList()
        delegate?.reloadData()
    }
    
    
    
    
    
    
    
    
}
