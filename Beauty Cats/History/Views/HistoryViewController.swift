//
//  HistoryViewController.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    let vm = HistoryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.delegate=self
        vm.getHistory()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HistoryViewController:HistoryViewModelDelegate{
    func loadData() {
        DispatchQueue.main.async {
            self.table.reloadData()
        }
    }
    
    func reloadData() {
        
    }
    
    func alert(msj: String) {
        
    }
    
    
}
extension HistoryViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HistoryTableViewCell
        cell.lblId.text="ID: \(item.id!)"
        cell.imgAction.image=UIImage(named: "broken-heart")
        cell.imgCat.image=UIImage(named:"")
        cell.imgCat.addRadius(round:cell.imgCat.frame.width/2 )
        cell.imgCat.load(strUrl: item.urlImagen!)
        cell.lblDate.text="Date: \(String(describing: item.date!))"
        if item.isLike{
            cell.imgAction.image=UIImage(named: "heart")
        }
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
}
