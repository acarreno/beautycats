//
//  HistoryRouting.swift
//  Beauty Cats
//
//  Created by iMac on 1/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
import UIKit

extension HistoryViewController{
    static func instantiate() -> HistoryViewController {
        let storyboard = UIStoryboard(name: "History", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HistoryViewController
        return vc
    }
    
}
