//
//  CardCell.swift
//  Beauty Cats
//
//  Created by iMac on 2/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
import VerticalCardSwiper
class CatCardCell: CardCell {

    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var imgStatus: UIImageView!
}
