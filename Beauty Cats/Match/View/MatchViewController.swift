//
//  MatchViewController.swift
//  Beauty Cats
//
//  Created by iMac on 2/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import UIKit
import VerticalCardSwiper
class MatchViewController: UIViewController {
    
    @IBOutlet weak var contentMatch: UIView!
    
    private var cardSwiper: VerticalCardSwiper!
    var vm = MatchViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        hideBackButton()
        self.navigationItem.title = "My Title"
    }
    
    func initSetup(){
        cardSwiper = VerticalCardSwiper(frame: view.bounds)
        
        view.addSubview(cardSwiper)
       
        cardSwiper.datasource = self
        cardSwiper.delegate = self
       
        cardSwiper.register(nib: UINib(nibName: "CatCardCell", bundle: nil), forCellWithReuseIdentifier: "CatCardCell")
        vm.delegate=self
        vm.getImages()
        hideBackButton()
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MatchViewController:MatchViewModelDelegate{
    func reloadData() {
        DispatchQueue.main.async {
            self.cardSwiper.insertCards(at: [0,1])
            self.cardSwiper.reloadData()
            self.cardSwiper.datasource = self
        }
    }
    
    func alert(msj: String) {
        
    }
    
    func removeCell(index: Int) {
        
    }
    
    
}
extension MatchViewController:VerticalCardSwiperDatasource {
    
    func cardForItemAt(verticalCardSwiperView: VerticalCardSwiperView, cardForItemAt index: Int) -> CardCell {
        let item=vm.container[index]
       
        if let cardCell = verticalCardSwiperView.dequeueReusableCell(withReuseIdentifier: "CatCardCell", for: index) as? CatCardCell {
            cardCell.image.image=UIImage(named:"")
            cardCell.image.load(strUrl:item.url )
            cardCell.imgStatus.image=UIImage(named:"")
            cardCell.contentView.addBoder()
            cardCell.contentView.addRadius(round: 10)
            return cardCell
            
        }
        return CardCell()
    }
    
    func numberOfCards(verticalCardSwiperView: VerticalCardSwiperView) -> Int {
        return vm.container.count
    }
}
extension MatchViewController: VerticalCardSwiperDelegate {
    func willSwipeCardAway(card: CardCell, index: Int, swipeDirection: SwipeDirection) {
        if index < vm.container.count {
            vm.container.remove(at: index)
            
        }
        
        
    }
    func didSwipeCardAway(card: CardCell, index: Int, swipeDirection: SwipeDirection) {
        let viewCart = card as!CatCardCell
        viewCart.imgStatus.image=UIImage(named:"")
        if swipeDirection.rawValue == 1{
            
            vm.vote(isLike: true, index: index)
           
        }else{
            vm.vote(isLike: false, index: index)
        }
        if vm.container.count==0{
            vm.container=[]
            vm.getImages()
        }
    }
    
    /*private func willSwipeCardAway(card: CardCell, index: Int, swipeDirection: CellSwipeDirection) {
    
        // called right before the card animates off the screen (optional).
    }

    func didSwipeCardAway(card: CardCell, index: Int, swipeDirection: CellSwipeDirection) {

        // handle swipe gestures (optional).
    }*/
    
    func didCancelSwipe(card: CardCell, index: Int) {
         let viewCart = card as!CatCardCell
        viewCart.imgStatus.image=UIImage(named:"")
        // Called when a card swipe is cancelled (when the threshold wasn't reached)
    }
    
    func sizeForItem(verticalCardSwiperView: VerticalCardSwiperView, index: Int) -> CGSize {
    
        // Allows you to return custom card sizes (optional).
        return CGSize(width: contentMatch.frame.width, height: contentMatch.frame.height )
    }
    
    func didScroll(verticalCardSwiperView: VerticalCardSwiperView) {
    
        // Tells the delegate when the user scrolls through the cards (optional).
    }
    
    func didEndScroll(verticalCardSwiperView: VerticalCardSwiperView) {
    
        // Tells the delegate when scrolling through the cards came to an end (optional).
    }
    
    func didDragCard(card: CardCell, index: Int, swipeDirection: SwipeDirection) {
        let viewCart = card as!CatCardCell
        print(swipeDirection.rawValue)
        viewCart.imgStatus.image = viewCart.imgStatus.image?.withRenderingMode(.alwaysTemplate)
        if swipeDirection.rawValue == 1{
            
            
            viewCart.imgStatus.image=UIImage(named:"heart")
           
            viewCart.imgStatus.tintColor = UIColor.red
        }else{
            viewCart.imgStatus.image=UIImage(named:"broken-heart")
        }
        
        // Called when the user starts dragging a card to the side (optional).
    }
    
    func didTapCard(verticalCardSwiperView: VerticalCardSwiperView, index: Int) {
    
        // Tells the delegate when the user taps a card (optional).
    }
    
    func didHoldCard(verticalCardSwiperView: VerticalCardSwiperView, index: Int, state: UIGestureRecognizer.State) {
    
        // Tells the delegate when the user holds a card (optional).
    }
}
