//
//  MatchManager.swift
//  Beauty Cats
//
//  Created by iMac on 2/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
class MatchManager {
    func getImages(url:String,page:Int,susses:@escaping ([CatModel])->(),failed:@escaping (String)->()){
        let url="\(url)?limit=10&page=\(page)&order=DESC"
        Services.get(url: url, susses: { (res) in
            if let list = res as? [[String:Any]]{
                
                if list.count>0{
                    
                    var listParc=[CatModel]()
                    
                    list.forEach { (item) in
                        listParc.append(try! DictionaryDecoder().decode(CatModel.self, from: (item) ))
                        
                    }
                    susses(listParc)
                }else{
                    failed("Not found cats")
                }
                
            }else{
                failed("Error, try again")
            }
        }) { (error) in
            
        }
        
    }
}
