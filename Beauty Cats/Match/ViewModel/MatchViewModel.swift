//
//  MatchViewModel.swift
//  Beauty Cats
//
//  Created by iMac on 2/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
protocol MatchViewModelDelegate: class {
    func reloadData()
    func alert(msj:String)
    func removeCell(index:Int)
}
class MatchViewModel {
    
    enum ids:String {
        case item="item"
        case pagination="pagination"
        
    }
    
    struct cellModel {
        let cell:ids
        
    }
    
    weak var delegate: MatchViewModelDelegate?
    
    var container=[CatModel]()
    var page=0
    var isLoading=false
    let manager=MatchManager()
    private let BDManager = CoreDataManager()
    
    func getImages(){
        page += 1
        manager.getImages(url: ApiVars.get.search,page:page,susses: {res in
            self.container=res
            self.delegate?.reloadData()
        }) { (error) in
            print(error)
        }
    }
    
    func vote(isLike:Bool,index:Int){
        if index < container.count-1{
            let item = container[index]
            BDManager.createRegister(id: item.id, url: item.url, isLike: isLike) {
                print("complete")
            }
        }
        
    }
    
}
