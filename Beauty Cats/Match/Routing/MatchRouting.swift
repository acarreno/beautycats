//
//  MatchRouting.swift
//  Beauty Cats
//
//  Created by iMac on 2/08/20.
//  Copyright © 2020 Andrey. All rights reserved.
//

import Foundation
import UIKit

extension MatchViewController{
    static func instantiate() -> MatchViewController {
        let storyboard = UIStoryboard(name: "Match", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MatchViewController
        return vc
    }
    
}
